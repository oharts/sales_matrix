require 'test_helper'

class GparametersControllerTest < ActionController::TestCase
  setup do
    @gparameter = gparameters(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:gparameters)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create gparameter" do
    assert_difference('Gparameter.count') do
      post :create, gparameter: { bed1: @gparameter.bed1, bed2: @gparameter.bed2, bed3: @gparameter.bed3, bed4: @gparameter.bed4, height_categories: @gparameter.height_categories, other: @gparameter.other, ph: @gparameter.ph, podium: @gparameter.podium, studio: @gparameter.studio, sub_ph: @gparameter.sub_ph, total: @gparameter.total, tower: @gparameter.tower, wiev_aspects: @gparameter.wiev_aspects }
    end

    assert_redirected_to gparameter_path(assigns(:gparameter))
  end

  test "should show gparameter" do
    get :show, id: @gparameter
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @gparameter
    assert_response :success
  end

  test "should update gparameter" do
    patch :update, id: @gparameter, gparameter: { bed1: @gparameter.bed1, bed2: @gparameter.bed2, bed3: @gparameter.bed3, bed4: @gparameter.bed4, height_categories: @gparameter.height_categories, other: @gparameter.other, ph: @gparameter.ph, podium: @gparameter.podium, studio: @gparameter.studio, sub_ph: @gparameter.sub_ph, total: @gparameter.total, tower: @gparameter.tower, wiev_aspects: @gparameter.wiev_aspects }
    assert_redirected_to gparameter_path(assigns(:gparameter))
  end

  test "should destroy gparameter" do
    assert_difference('Gparameter.count', -1) do
      delete :destroy, id: @gparameter
    end

    assert_redirected_to gparameters_path
  end
end
