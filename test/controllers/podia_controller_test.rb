require 'test_helper'

class PodiaControllerTest < ActionController::TestCase
  setup do
    @podium = podia(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:podia)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create podium" do
    assert_difference('Podium.count') do
      post :create, podium: { apt2: @podium.apt2, apt10: @podium.apt10, apt11: @podium.apt11, apt12: @podium.apt12, apt13: @podium.apt13, apt14: @podium.apt14, apt15: @podium.apt15, apt16: @podium.apt16, apt17: @podium.apt17, apt18: @podium.apt18, apt19: @podium.apt19, apt1: @podium.apt1, apt20: @podium.apt20, apt3: @podium.apt3, apt4: @podium.apt4, apt5: @podium.apt5, apt6: @podium.apt6, apt7: @podium.apt7, apt8: @podium.apt8, apt9: @podium.apt9 }
    end

    assert_redirected_to podium_path(assigns(:podium))
  end

  test "should show podium" do
    get :show, id: @podium
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @podium
    assert_response :success
  end

  test "should update podium" do
    patch :update, id: @podium, podium: { apt2: @podium.apt2, apt10: @podium.apt10, apt11: @podium.apt11, apt12: @podium.apt12, apt13: @podium.apt13, apt14: @podium.apt14, apt15: @podium.apt15, apt16: @podium.apt16, apt17: @podium.apt17, apt18: @podium.apt18, apt19: @podium.apt19, apt1: @podium.apt1, apt20: @podium.apt20, apt3: @podium.apt3, apt4: @podium.apt4, apt5: @podium.apt5, apt6: @podium.apt6, apt7: @podium.apt7, apt8: @podium.apt8, apt9: @podium.apt9 }
    assert_redirected_to podium_path(assigns(:podium))
  end

  test "should destroy podium" do
    assert_difference('Podium.count', -1) do
      delete :destroy, id: @podium
    end

    assert_redirected_to podia_path
  end
end
