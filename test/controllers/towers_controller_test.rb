require 'test_helper'

class TowersControllerTest < ActionController::TestCase
  setup do
    @tower = towers(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:towers)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create tower" do
    assert_difference('Tower.count') do
      post :create, tower: { apt10: @tower.apt10, apt11: @tower.apt11, apt12: @tower.apt12, apt13: @tower.apt13, apt14: @tower.apt14, apt15: @tower.apt15, apt16: @tower.apt16, apt17: @tower.apt17, apt18: @tower.apt18, apt19: @tower.apt19, apt1: @tower.apt1, apt20: @tower.apt20, apt2: @tower.apt2, apt3: @tower.apt3, apt4: @tower.apt4, apt5: @tower.apt5, apt6: @tower.apt6, apt7: @tower.apt7, apt8: @tower.apt8, apt9: @tower.apt9 }
    end

    assert_redirected_to tower_path(assigns(:tower))
  end

  test "should show tower" do
    get :show, id: @tower
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @tower
    assert_response :success
  end

  test "should update tower" do
    patch :update, id: @tower, tower: { apt10: @tower.apt10, apt11: @tower.apt11, apt12: @tower.apt12, apt13: @tower.apt13, apt14: @tower.apt14, apt15: @tower.apt15, apt16: @tower.apt16, apt17: @tower.apt17, apt18: @tower.apt18, apt19: @tower.apt19, apt1: @tower.apt1, apt20: @tower.apt20, apt2: @tower.apt2, apt3: @tower.apt3, apt4: @tower.apt4, apt5: @tower.apt5, apt6: @tower.apt6, apt7: @tower.apt7, apt8: @tower.apt8, apt9: @tower.apt9 }
    assert_redirected_to tower_path(assigns(:tower))
  end

  test "should destroy tower" do
    assert_difference('Tower.count', -1) do
      delete :destroy, id: @tower
    end

    assert_redirected_to towers_path
  end
end
