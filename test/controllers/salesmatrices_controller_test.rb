require 'test_helper'

class SalesmatricesControllerTest < ActionController::TestCase
  setup do
    @salesmatrix = salesmatrices(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:salesmatrices)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create salesmatrix" do
    assert_difference('Salesmatrix.count') do
      post :create, salesmatrix: { created_by: @salesmatrix.created_by, pricing_version: @salesmatrix.pricing_version, project_name: @salesmatrix.project_name }
    end

    assert_redirected_to salesmatrix_path(assigns(:salesmatrix))
  end

  test "should show salesmatrix" do
    get :show, id: @salesmatrix
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @salesmatrix
    assert_response :success
  end

  test "should update salesmatrix" do
    patch :update, id: @salesmatrix, salesmatrix: { created_by: @salesmatrix.created_by, pricing_version: @salesmatrix.pricing_version, project_name: @salesmatrix.project_name }
    assert_redirected_to salesmatrix_path(assigns(:salesmatrix))
  end

  test "should destroy salesmatrix" do
    assert_difference('Salesmatrix.count', -1) do
      delete :destroy, id: @salesmatrix
    end

    assert_redirected_to salesmatrices_path
  end
end
