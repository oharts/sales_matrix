require 'test_helper'

class ApartmentsControllerTest < ActionController::TestCase
  setup do
    @apartment = apartments(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:apartments)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create apartment" do
    assert_difference('Apartment.count') do
      post :create, apartment: { apartmentID: @apartment.apartmentID, apartment_type: @apartment.apartment_type, balcony_area: @apartment.balcony_area, base_price: @apartment.base_price, bathrooms: @apartment.bathrooms, bedrooms: @apartment.bedrooms, carbays: @apartment.carbays, colour: @apartment.colour, internal_area: @apartment.internal_area, name: @apartment.name, study: @apartment.study, total_area: @apartment.total_area }
    end

    assert_redirected_to apartment_path(assigns(:apartment))
  end

  test "should show apartment" do
    get :show, id: @apartment
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @apartment
    assert_response :success
  end

  test "should update apartment" do
    patch :update, id: @apartment, apartment: { apartmentID: @apartment.apartmentID, apartment_type: @apartment.apartment_type, balcony_area: @apartment.balcony_area, base_price: @apartment.base_price, bathrooms: @apartment.bathrooms, bedrooms: @apartment.bedrooms, carbays: @apartment.carbays, colour: @apartment.colour, internal_area: @apartment.internal_area, name: @apartment.name, study: @apartment.study, total_area: @apartment.total_area }
    assert_redirected_to apartment_path(assigns(:apartment))
  end

  test "should destroy apartment" do
    assert_difference('Apartment.count', -1) do
      delete :destroy, id: @apartment
    end

    assert_redirected_to apartments_path
  end
end
