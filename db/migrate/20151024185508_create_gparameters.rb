class CreateGparameters < ActiveRecord::Migration
  def change
    create_table :gparameters do |t|
      t.integer :podium
      t.integer :tower
      t.integer :total
      t.integer :studio
      t.integer :bed1
      t.integer :bed2
      t.integer :bed3
      t.integer :bed4
      t.integer :sub_ph
      t.integer :ph
      t.integer :other
      t.integer :height_categories
      t.integer :wiev_aspects

      t.timestamps null: false
    end
  end
end
