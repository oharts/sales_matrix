class CreateApartments < ActiveRecord::Migration
  def change
    create_table :apartments do |t|
      t.string :apartmentID
      t.string :apartment_type
      t.string :name
      t.string :internal_area
      t.string :balcony_area
      t.string :total_area
      t.string :bedrooms
      t.string :bathrooms
      t.string :study
      t.string :carbays
      t.string :base_price
      t.string :colour

      t.timestamps null: false
    end
  end
end
