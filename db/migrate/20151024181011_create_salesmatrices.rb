class CreateSalesmatrices < ActiveRecord::Migration
  def change
    create_table :salesmatrices do |t|
      t.string :project_name
      t.string :pricing_version
      t.string :created_by

      t.timestamps null: false
    end
  end
end
