class ChangeStringInteger < ActiveRecord::Migration
  def change
    change_column :apartments, :internal_area, :integer
    change_column :apartments, :balcony_area, :integer
    change_column :apartments, :total_area, :integer
    change_column :apartments, :bedrooms, :integer
    change_column :apartments, :bathrooms, :integer
    change_column :apartments, :study, :integer
    change_column :apartments, :carbays, :integer
    change_column :apartments, :base_price, :integer
  end
end
