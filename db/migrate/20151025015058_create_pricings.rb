class CreatePricings < ActiveRecord::Migration
  def change
    create_table :pricings do |t|
      t.integer :level_pr
      t.integer :carpark_pr
      t.integer :height_pr
      t.integer :aspect_pr

      t.timestamps null: false
    end
  end
end
