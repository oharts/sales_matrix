class CreateTowers < ActiveRecord::Migration
  def change
    create_table :towers do |t|
      t.string :apt1
      t.string :apt2
      t.string :apt3
      t.string :apt4
      t.string :apt5
      t.string :apt6
      t.string :apt7
      t.string :apt8
      t.string :apt9
      t.string :apt10
      t.string :apt11
      t.string :apt12
      t.string :apt13
      t.string :apt14
      t.string :apt15
      t.string :apt16
      t.string :apt17
      t.string :apt18
      t.string :apt19
      t.string :apt20

      t.timestamps null: false
    end
  end
end
