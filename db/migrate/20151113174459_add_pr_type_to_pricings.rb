class AddPrTypeToPricings < ActiveRecord::Migration
  def change
    add_column :pricings, :carpark_pr_type, :integer
    add_column :pricings, :height_pr_type, :integer
    add_column :pricings, :aspect_pr_type, :integer
    
  end
end
