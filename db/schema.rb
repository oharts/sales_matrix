# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151113174459) do

  create_table "apartments", force: :cascade do |t|
    t.string   "apartmentID"
    t.string   "apartment_type"
    t.string   "name"
    t.integer  "internal_area"
    t.integer  "balcony_area"
    t.integer  "total_area"
    t.integer  "bedrooms"
    t.integer  "bathrooms"
    t.integer  "study"
    t.integer  "carbays"
    t.integer  "base_price"
    t.string   "colour"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "gparameters", force: :cascade do |t|
    t.integer  "podium"
    t.integer  "tower"
    t.integer  "total"
    t.integer  "studio"
    t.integer  "bed1"
    t.integer  "bed2"
    t.integer  "bed3"
    t.integer  "bed4"
    t.integer  "sub_ph"
    t.integer  "ph"
    t.integer  "other"
    t.integer  "height_categories"
    t.integer  "wiev_aspects"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  create_table "podia", force: :cascade do |t|
    t.string   "apt1"
    t.string   "apt2"
    t.string   "apt3"
    t.string   "apt4"
    t.string   "apt5"
    t.string   "apt6"
    t.string   "apt7"
    t.string   "apt8"
    t.string   "apt9"
    t.string   "apt10"
    t.string   "apt11"
    t.string   "apt12"
    t.string   "apt13"
    t.string   "apt14"
    t.string   "apt15"
    t.string   "apt16"
    t.string   "apt17"
    t.string   "apt18"
    t.string   "apt19"
    t.string   "apt20"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "pricings", force: :cascade do |t|
    t.integer  "level_pr"
    t.integer  "carpark_pr"
    t.integer  "height_pr"
    t.integer  "aspect_pr"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "level_pr_type"
    t.integer  "carpark_pr_type"
    t.integer  "height_pr_type"
    t.integer  "aspect_pr_type"
  end

  create_table "salesmatrices", force: :cascade do |t|
    t.string   "project_name"
    t.string   "pricing_version"
    t.string   "created_by"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "towers", force: :cascade do |t|
    t.string   "apt1"
    t.string   "apt2"
    t.string   "apt3"
    t.string   "apt4"
    t.string   "apt5"
    t.string   "apt6"
    t.string   "apt7"
    t.string   "apt8"
    t.string   "apt9"
    t.string   "apt10"
    t.string   "apt11"
    t.string   "apt12"
    t.string   "apt13"
    t.string   "apt14"
    t.string   "apt15"
    t.string   "apt16"
    t.string   "apt17"
    t.string   "apt18"
    t.string   "apt19"
    t.string   "apt20"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
