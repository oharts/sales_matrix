json.array!(@apartments) do |apartment|
  json.extract! apartment, :id, :apartmentID, :apartment_type, :name, :internal_area, :balcony_area, :total_area, :bedrooms, :bathrooms, :study, :carbays, :base_price, :colour
  json.url apartment_url(apartment, format: :json)
end
