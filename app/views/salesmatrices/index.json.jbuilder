json.array!(@salesmatrices) do |salesmatrix|
  json.extract! salesmatrix, :id, :project_name, :pricing_version, :created_by
  json.url salesmatrix_url(salesmatrix, format: :json)
end
