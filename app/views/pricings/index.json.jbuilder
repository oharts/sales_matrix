json.array!(@pricings) do |pricing|
  json.extract! pricing, :id, :level_pr, :carpark_pr, :height_pr, :aspect_pr
  json.url pricing_url(pricing, format: :json)
end
