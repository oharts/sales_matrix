json.array!(@gparameters) do |gparameter|
  json.extract! gparameter, :id, :podium, :tower, :total, :studio, :bed1, :bed2, :bed3, :bed4, :sub_ph, :ph, :other, :height_categories, :wiev_aspects
  json.url gparameter_url(gparameter, format: :json)
end
