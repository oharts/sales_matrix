json.array!(@podia) do |podium|
  json.extract! podium, :id, :apt1, :apt2, :apt3, :apt4, :apt5, :apt6, :apt7, :apt8, :apt9, :apt10, :apt11, :apt12, :apt13, :apt14, :apt15, :apt16, :apt17, :apt18, :apt19, :apt20
  json.url podium_url(podium, format: :json)
end
