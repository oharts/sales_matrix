class Apartment < ActiveRecord::Base
    
    before_save :set_total_area
before_save :def_val
  before_validation :def_val
  
  def def_val
    self.bathrooms ||= 0
    self.study ||= 0
    self.carbays ||= 0
  end
 
  def set_total_area
 unless !self.internal_area.nil?
 self.internal_area=0
 end
 unless !self.balcony_area.nil?
 self.balcony_area=0
 end
    self.total_area = self.internal_area+self.balcony_area

  end
    validates_inclusion_of  :bathrooms, :in => 0..10, :message => "Value must be beetween 0 - 10!"
    validates_inclusion_of  :study, :in => 0..5, :message => "Value must be beetween 0 - 5!"
    validates_inclusion_of  :carbays, :in => 0..10, :message => "Value must be beetween 0 - 10!"
      
end
