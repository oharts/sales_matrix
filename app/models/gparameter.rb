class Gparameter < ActiveRecord::Base
    
    attr_writer :current_step
    before_save :def_val
  before_validation :def_val
  
  def def_val
    self.podium ||= 0
    self.tower ||= 1
    self.height_categories ||= 0
    self.wiev_aspects ||= 0
    self.studio ||= 0
    self.bed1 ||= 0
    self.bed2 ||= 0
    self.bed3 ||= 0
    self.bed4 ||= 0
    self.sub_ph ||= 0
    self.ph ||= 0
    self.other ||= 0
  end
  
    
  def current_step
    @current_step || steps.first
  end
  
  def steps
    %w[storeys variaties outline]
  end
  
  def next_step
    self.current_step = steps[steps.index(current_step)+1]
  end
  
  def previous_step
    self.current_step = steps[steps.index(current_step)-1]
  end
  
  def first_step?
    current_step == steps.first
  end
  
  def last_step?
    current_step == steps.last
  end
    
    def all_valid?
    steps.all? do |step|
      self.current_step = step
      valid?
    end
  end
  
  validates_inclusion_of  :podium, :in => 0..100, :message => "Value must be beetween 0 - 100!"
  validates_inclusion_of  :tower, :in => 0..100, :message => "Value must be beetween 0 - 100!"
  validates_inclusion_of  :height_categories, :in => 0..4, :message => "Value must be beetween 0 - 100!"
  validates_inclusion_of  :studio, :in => 0..50, :message => "Value must be beetween 0 - 50!"
  validates_inclusion_of  :bed1, :in => 0..50, :message => "Value must be beetween 0 - 50!"
  validates_inclusion_of  :bed2, :in => 0..50, :message => "Value must be beetween 0 - 50!"
  validates_inclusion_of  :bed3, :in => 0..50, :message => "Value must be beetween 0 - 50!"
  validates_inclusion_of  :bed4, :in => 0..50, :message => "Value must be beetween 0 - 50!"
  validates_inclusion_of  :sub_ph, :in => 0..50, :message => "Value must be beetween 0 - 50!"
  validates_inclusion_of  :ph, :in => 0..50, :message => "Value must be beetween 0 - 50!"
  validates_inclusion_of  :other, :in => 0..50, :message => "Value must be beetween 0 - 50!"


end
