class GparametersController < ApplicationController
  before_action :set_gparameter, only: [:show, :edit, :update, :destroy]

  # GET /gparameters
  # GET /gparameters.json
  def index
    @gparameters = Gparameter.all
  end

  # GET /gparameters/1
  # GET /gparameters/1.json
  def show
  end

  # GET /gparameters/new
  def new
    session[:gparameter_params] ||= {}
    @gparameter = Gparameter.new(session[:gparameter_params])
    @gparameter.current_step = session[:gparameter_step]
  end

  # GET /gparameters/1/edit
  def edit
  end

  # POST /gparameters
  # POST /gparameters.json
  def create
    session[:gparameter_params].deep_merge!(params[:gparameter]) if params[:gparameter]
    @gparameter = Gparameter.new(session[:gparameter_params])
    @gparameter.current_step = session[:gparameter_step]
    if @gparameter.valid?
      if params[:back_button]
        @gparameter.previous_step
      elsif @gparameter.last_step?
        @gparameter.save if @gparameter.all_valid?
      else
        @gparameter.next_step
      end
      session[:gparameter_step] = @gparameter.current_step
    end
    if @gparameter.new_record?
      render "new"
    else
      session[:gparameter_step] = session[:gparameter_params] = nil
      #flash[:notice] = "XXXXXXX!"
      redirect_to ap_path
    end
  end

  # PATCH/PUT /gparameters/1
  # PATCH/PUT /gparameters/1.json
  def update
    respond_to do |format|
      if @gparameter.update(gparameter_params)
        format.html { redirect_to @gparameter, notice: 'Gparameter was successfully updated.' }
        format.json { render :show, status: :ok, location: @gparameter }
      else
        format.html { render :edit }
        format.json { render json: @gparameter.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /gparameters/1
  # DELETE /gparameters/1.json
  def destroy
    @gparameter.destroy
    respond_to do |format|
      format.html { redirect_to gparameters_url, notice: 'Gparameter was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_gparameter
      @gparameter = Gparameter.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def gparameter_params
      params.require(:gparameter).permit(:podium, :tower, :total, :studio, :bed1, :bed2, :bed3, :bed4, :sub_ph, :ph, :other, :height_categories, :wiev_aspects)
    end
end
