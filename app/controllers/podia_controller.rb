class PodiaController < ApplicationController
  before_action :set_podium, only: [:show, :edit, :update, :destroy]

  # GET /podia
  # GET /podia.json
  def index
    @podia = Podium.all
  end

  # GET /podia/1
  # GET /podia/1.json
  def show
  end

  # GET /podia/new
  def new
    @podium = Podium.create
    flash[:notice] = "New layout was successfully created."
    redirect_to(:action => 'index')
     
    
    
  end

  # GET /podia/1/edit
  def edit
  end

  # POST /podia
  # POST /podia.json
  def create
    @podium = Podium.new(podium_params)

    respond_to do |format|
      if @podium.save
        
        redirect_to(:action => 'index')
      else
        render('new')
        
        
      #   format.html { redirect_to @podium, notice: 'Podium was successfully created.' }
      #   format.json { render :show, status: :created, location: @podium }
      # else
      #   format.html { render :new }
      #   format.json { render json: @podium.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /podia/1
  # PATCH/PUT /podia/1.json
  def update
    respond_to do |format|
      if @podium.update(podium_params)
        format.html { redirect_to @podium, notice: 'Podium was successfully updated.' }
        format.json { render :show, status: :ok, location: @podium }
      else
        format.html { render :edit }
        format.json { render json: @podium.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def del_all
  Podium.delete_all
  flash[:notice] = "All Layouts deleted!"
  redirect_to(:action => 'index')
  end

  # DELETE /podia/1
  # DELETE /podia/1.json
  def destroy
    @podium.destroy
    respond_to do |format|
      format.html { redirect_to podia_url, notice: 'Layout was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_podium
      @podium = Podium.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def podium_params
      params.require(:podium).permit(:apt1, :apt2, :apt3, :apt4, :apt5, :apt6, :apt7, :apt8, :apt9, :apt10, :apt11, :apt12, :apt13, :apt14, :apt15, :apt16, :apt17, :apt18, :apt19, :apt20)
    end
end
