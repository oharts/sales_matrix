class SalesmatricesController < ApplicationController
  before_action :set_salesmatrix, only: [:show, :edit, :update, :destroy]

  # GET /salesmatrices
  # GET /salesmatrices.json
  def index
    @salesmatrices = Salesmatrix.all
  end

  # GET /salesmatrices/1
  # GET /salesmatrices/1.json
  def show
  end

  # GET /salesmatrices/new
  def new
    @salesmatrix = Salesmatrix.new
  end

  # GET /salesmatrices/1/edit
  def edit
  end

  # POST /salesmatrices
  # POST /salesmatrices.json
  def create
    @salesmatrix = Salesmatrix.new(salesmatrix_params)

    respond_to do |format|
      if @salesmatrix.save
        format.html { redirect_to @salesmatrix, notice: 'Salesmatrix was successfully created.' }
        format.json { render :show, status: :created, location: @salesmatrix }
      else
        format.html { render :new }
        format.json { render json: @salesmatrix.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /salesmatrices/1
  # PATCH/PUT /salesmatrices/1.json
  def update
    respond_to do |format|
      if @salesmatrix.update(salesmatrix_params)
        format.html { redirect_to @salesmatrix, notice: 'Salesmatrix was successfully updated.' }
        format.json { render :show, status: :ok, location: @salesmatrix }
      else
        format.html { render :edit }
        format.json { render json: @salesmatrix.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /salesmatrices/1
  # DELETE /salesmatrices/1.json
  def destroy
    @salesmatrix.destroy
    respond_to do |format|
      format.html { redirect_to salesmatrices_url, notice: 'Salesmatrix was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_salesmatrix
      @salesmatrix = Salesmatrix.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def salesmatrix_params
      params.require(:salesmatrix).permit(:project_name, :pricing_version, :created_by)
    end
end
