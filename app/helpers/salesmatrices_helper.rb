module SalesmatricesHelper
    def delete_old_data
        Gparameter.delete_all
        Podium.delete_all
        Tower.delete_all
        Apartment.delete_all
        Salesmatrix.delete_all
        Pricing.delete_all
    end
    
    
end
